﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public PlayerMovement movement;

    // When the PLAYER hits an OBSTACLE
    private void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Obstacle")
        {
            Debug.Log("We hit something");
            movement.enabled = false;
            FindObjectOfType<GameManager>().EndGame();
        }
    }
}
