﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelComplete : MonoBehaviour
{

    public void LoadScene()
    {
        int y = SceneManager.GetActiveScene().buildIndex; // Gets the Index of the active scene to int "y"
        if (y == 3)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 3);
        }

        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            Debug.Log("LEVEL WON!");
        }
    }
}
