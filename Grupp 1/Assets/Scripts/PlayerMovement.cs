﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb;
    public float movmentForce = 500F;
    public float sidewaysForce = 100f;

    void FixedUpdate()
    {

        // Right Movment Key
        if (Input.GetKey("d"))
        {
            rb.AddForce(0, 0, -sidewaysForce * Time.deltaTime, ForceMode.VelocityChange); 
        }
        // Left Movment Key
        if (Input.GetKey("a"))
        {
            rb.AddForce(0, 0, sidewaysForce * Time.deltaTime, ForceMode.VelocityChange);
        }
        // Forward Movment Key
        if (Input.GetKey("w"))
        {
            rb.AddForce(movmentForce * Time.deltaTime, 0, 0);
        }

        // If player falls from the Ground Platform
        if (rb.position.y < -50f)
        {
            FindObjectOfType<GameManager>().EndGame();  // Restarts the Level
        }


    }






}
