﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTrigger : MonoBehaviour
{
    public GameManager gameManager;

    // If player reaches the Goal
    void OnTriggerEnter()
    {
        gameManager.CompleteLevel();
    }
}
