﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayMap01()
    {
        SceneManager.LoadScene("Map01");
    }

    public void PlayMap02()
    {
        SceneManager.LoadScene("Map02");
    }

    public void PlayMap03()
    {
        SceneManager.LoadScene("Map03");
    }

    public void QuitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }

}
